if has("autocmd") && exists("+omnifunc")     
    autocmd Filetype octave  
                \ if &omnifunc == "" |    
                \ setlocal omnifunc=syntaxcomplete#Complete |          
                \ endif  
endif           
